// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;
using System.Collections.Generic;

public class Death_in_the_OrientEditorTarget : TargetRules
{
	public Death_in_the_OrientEditorTarget(TargetInfo Target) : base(Target)
	{
		Type = TargetType.Editor;
		ExtraModuleNames.Add("Death_in_the_Orient");
	}
}
