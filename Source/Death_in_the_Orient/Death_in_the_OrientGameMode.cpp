// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#include "Death_in_the_OrientGameMode.h"
#include "Death_in_the_OrientPlayerController.h"
#include "Death_in_the_OrientCharacter.h"
#include "UObject/ConstructorHelpers.h"

ADeath_in_the_OrientGameMode::ADeath_in_the_OrientGameMode()
{
	// use our custom PlayerController class
	PlayerControllerClass = ADeath_in_the_OrientPlayerController::StaticClass();

	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/TopDownCPP/Blueprints/TopDownCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}