// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "Death_in_the_OrientGameMode.generated.h"

UCLASS(minimalapi)
class ADeath_in_the_OrientGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	ADeath_in_the_OrientGameMode();
};



