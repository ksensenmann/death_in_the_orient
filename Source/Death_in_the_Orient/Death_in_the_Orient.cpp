// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#include "Death_in_the_Orient.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, Death_in_the_Orient, "Death_in_the_Orient" );

DEFINE_LOG_CATEGORY(LogDeath_in_the_Orient)
 