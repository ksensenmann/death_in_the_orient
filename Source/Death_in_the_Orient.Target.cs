// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;
using System.Collections.Generic;

public class Death_in_the_OrientTarget : TargetRules
{
	public Death_in_the_OrientTarget(TargetInfo Target) : base(Target)
	{
		Type = TargetType.Game;
		ExtraModuleNames.Add("Death_in_the_Orient");
	}
}
